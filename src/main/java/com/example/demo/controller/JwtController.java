package com.example.demo.controller;

import com.example.demo.common.utils.encryption.JWTUtil;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author
 * @Des TODO
 * @Date 2023/2/14 16:58
 **/
@RestController
@RequestMapping("/jwt")
public class JwtController {
    /**
     * 获取Token
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/get")
    public String getToken(@RequestParam Long id) {
        return JWTUtil.createToken(id.toString(), null);
    }
    
    /**
     * 验证Token
     *
     * @param token
     * @return
     */
    @PostMapping("/verify")
    public Map<String, String> verify(@RequestParam String token) {
        Map<String, String> map = new HashMap<>();
        String valid = JWTUtil.verifyToken(token);
        map.put("is_valid", valid);
        return map;
    }
}
