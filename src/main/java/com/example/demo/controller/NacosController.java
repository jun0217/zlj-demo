// package com.example.demo.controller;
//
// import com.alibaba.nacos.api.annotation.NacosInjected;
// import com.alibaba.nacos.api.config.annotation.NacosValue;
// import com.alibaba.nacos.api.naming.NamingService;
// import io.swagger.annotations.Api;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.beans.factory.annotation.Value;
// import org.springframework.cloud.client.ServiceInstance;
// import org.springframework.cloud.client.discovery.DiscoveryClient;
// import org.springframework.stereotype.Controller;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestMethod;
// import org.springframework.web.bind.annotation.ResponseBody;
//
// import java.util.List;
//
//
// /**
//  * @author 14589
//  * @Des
//  * @Date 2023/5/9 16:24
//  **/
// @Api(tags = "NacosController", description = "Nacos测试类管理")
// @Controller
// @RequestMapping("/nacos")
// public class NacosController {
//
//     @NacosInjected
//     private NamingService namingService;
//
//     @NacosValue(value = "${test.num:1}", autoRefreshed = true)
//     private String nacosValue;
//     @Value("${test.num:2}")
//     private String testValue;
//
//     // @Autowired
//     // private RestTemplate restTemplate;
//     // @RequestMapping(value = "/get01", method =RequestMethod.GET)
//     // @ResponseBody
//     // public String get(String url){
//     //     ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class);
//     //     return responseEntity.getBody();
//     // }
//
//     @Autowired
//     private DiscoveryClient discoveryClient;
//     @RequestMapping(value = "/get02", method = RequestMethod.GET)
//     @ResponseBody
//     public String getServiceUrl(String serviceName) {
//         List<ServiceInstance> instances = discoveryClient.getInstances(serviceName);
//         if (instances == null || instances.size() == 0) {
//             return null;
//         }
//         ServiceInstance instance = instances.get(0);
//         return instance.getUri().toString();
//     }
// }
