package com.example.demo.common.util;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.common.enums.ErrorEnum;

import java.math.BigDecimal;

/**
 * 本后台接口系统常用的json工具类
 */
public class CommonUtil {
    
    private static final String CODE = "code";
    private static final String INFO = "info";
    private static final String DATA = "data";
    public static final String MESSAGE = "message";
    
    private static final String PAGE_NUM = "pageNum";
    private static final String PAGE_ROW = "pageRow";
    private static final String PAGE_SIZE = "pageSize";
    
    private static final String SEPARATION_CHARACTER = ",";
    
    /**
     * 返回一个info为空对象的成功消息的json
     */
    public static JSONObject successJson() {
        return successJson(new JSONObject());
    }
    
    /**
     * 返回一个返回码为100的json
     */
    public static JSONObject successJson(Object info) {
        JSONObject resultJson = new JSONObject();
        resultJson.put(CODE, "200");
        resultJson.put(MESSAGE, "请求成功");
        resultJson.put(INFO, info);
        return resultJson;
    }
    
    /**
     * 返回错误信息JSON
     */
    public static JSONObject errorJson(ErrorEnum errorEnum) {
        JSONObject resultJson = new JSONObject();
        resultJson.put(CODE, errorEnum.getCode());
        resultJson.put(MESSAGE, errorEnum.getMessage());
        resultJson.put(INFO, new JSONObject());
        return resultJson;
    }
    
    
    /**
     * 在分页查询之前,为查询条件里加上分页参数
     *
     * @param paramObject    查询条件json
     * @param defaultPageRow 默认的每页条数,即前端不传pageRow参数时的每页条数
     */
    private static void fillPageParam(final JSONObject paramObject, int defaultPageRow) {
        int pageNum = paramObject.getIntValue(PAGE_NUM);
        pageNum = pageNum == 0 ? 1 : pageNum;
        int pageSize = paramObject.getIntValue(PAGE_SIZE);
        pageSize = pageSize == 0 ? defaultPageRow : pageSize;
        paramObject.put("offSet", (pageNum - 1) * pageSize);
        paramObject.put(PAGE_NUM, pageNum);
        paramObject.put(PAGE_ROW, pageSize);
        // 删除此参数,防止前端传了这个参数,pageHelper分页插件检测到之后,拦截导致SQL错误
        paramObject.remove(PAGE_SIZE);
        
    }
    
    /**
     * 分页查询之前的处理参数
     * 没有传pageRow参数时,默认每页10条.
     */
    public static void fillPageParam(final JSONObject paramObject) {
        fillPageParam(paramObject, 10);
    }
    
    /**
     * 校验密码强度（密码必须包含数字，英文，特殊字符三种组合方式）
     *
     * @param password
     * @return
     */
    public static boolean checkPassword(String password) {
        String regex = "^(?![a-zA-Z]+$)(?![A-Z0-9]+$)(?![A-Z\\W_]+$)(?![a-z0-9]+$)(?![a-z\\W_]+$)(?![0-9\\W_]+$)[a-zA-Z0-9\\W_]{6,18}$";
        if (!password.matches(regex)) {
            return false;
        }
        return true;
    }
    
    public static BigDecimal avgCal(BigDecimal busi, Long total, int scale) {
        if (total.compareTo(0L) == 0) {
            return BigDecimal.ZERO;
        }
        return busi.divide(BigDecimal.valueOf(total), scale, BigDecimal.ROUND_HALF_UP);
    }
}
