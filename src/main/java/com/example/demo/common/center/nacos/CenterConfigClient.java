package com.example.demo.common.center.nacos;

import org.springframework.beans.factory.annotation.Value;
// import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * 获取Nacos中的配置样例类
 **/
@Component
// @RefreshScope
public class CenterConfigClient {
    
    // @Value获取最新值一定要加@RefreshScope注解，配置文件中配置refresh: true
    @Value(value = "${test.num:1}")
    private String value;
    
    public String getValue() {
        return value;
    }
    
}
