package com.example.demo.common.config.filter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 数据共享 token 配置
 */
@Component
public class TokenConfiguration {
    
    public static String mdSalt;
    
    @Value("${token.share.salt}")
    public void setMdSalt(String mdSalt) {
        TokenConfiguration.mdSalt = mdSalt;
    }
}
