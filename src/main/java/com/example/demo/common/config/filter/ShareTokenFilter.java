package com.example.demo.common.config.filter;

import com.alibaba.fastjson.JSON;
import com.example.demo.common.utils.encryption.JWTUtil;
import com.fasterxml.jackson.core.filter.TokenFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * token 限制共享接口过滤器
 */
@Component
@WebFilter(filterName = "tokenFilter", urlPatterns = {"/share/**"})
public class ShareTokenFilter implements Filter {
    
    private static final Logger logger = LoggerFactory.getLogger(TokenFilter.class);
    
    public List<String> excludes = new ArrayList<>();
    
    private final String TOKEN = "token";
    
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // 非share开头的需要过滤掉
        excludes.add("(?!/share).*");
    }
    
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        if (handleExcludeURL(request, response)) {
            filterChain.doFilter(request, response);
            return;
        }
        //token使用的yml中的值
        String token = request.getHeader(TOKEN);
        String userNameFromToken = JWTUtil.verifyToken(token);
        if (!(TokenConfiguration.mdSalt).equals(userNameFromToken)) {
            logger.error("token认证失败");
            logger.error(userNameFromToken);
            PrintWriter out = null;
            try {
                response.setCharacterEncoding("UTF-8");
                response.setContentType("application/json");
                out = response.getWriter();
                out.println(JSON.toJSONString("token认证失败"));
            } finally {
                out.flush();
                out.close();
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
        return;
    }
    
    @Override
    public void destroy() {
    
    }
    
    private boolean handleExcludeURL(HttpServletRequest request, HttpServletResponse response) {
        
        if (excludes == null || excludes.isEmpty()) {
            return false;
        }
        
        String url = request.getServletPath();
        for (String pattern : excludes) {
            Pattern p = Pattern.compile("^" + pattern);
            Matcher m = p.matcher(url);
            if (m.find()) {
                return true;
            }
        }
        
        return false;
    }
}
