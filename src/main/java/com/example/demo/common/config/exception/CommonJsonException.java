package com.example.demo.common.config.exception;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.common.enums.ErrorEnum;

/**
 * 自定义错误类
 * 比如在校验参数时,如果不符合要求,可以抛出此错误类
 * 拦截器可以统一拦截此错误,将其中json返回给前端
 */
public class CommonJsonException extends RuntimeException {
    
    private static final String CODE = "code";
    private static final String DATA = "data";
    private static final String MESSAGE = "message";
    
    private JSONObject resultJson;
    
    public CommonJsonException(ErrorEnum errorEnum) {
        this.resultJson = errorJson(errorEnum);
    }
    
    public CommonJsonException(ErrorEnum errorEnum, String msg) {
        this.resultJson = errorJson(errorEnum);
        this.resultJson.put(MESSAGE, msg);
    }
    
    public CommonJsonException(ErrorEnum errorEnum, String msg, Object data) {
        this.resultJson = errorJson(errorEnum);
        this.resultJson.put(MESSAGE, msg);
        this.resultJson.put(DATA, data);
    }
    
    public CommonJsonException(ErrorEnum errorEnum, String msg, boolean needSysInfo) {
        this.resultJson = errorJson(errorEnum);
        if (needSysInfo) {
            this.resultJson.put(MESSAGE, errorEnum.getMessage() + msg);
        } else {
            this.resultJson.put(MESSAGE, msg);
        }
    }
    
    public <T> CommonJsonException(ErrorEnum errorEnum, T data) {
        this.resultJson = errorJson(errorEnum);
        this.resultJson.put(DATA, data);
    }
    
    public CommonJsonException(JSONObject resultJson) {
        this.resultJson = resultJson;
    }
    
    public JSONObject getResultJson() {
        return resultJson;
    }
    
    private static JSONObject errorJson(ErrorEnum errorEnum) {
        JSONObject resultJson = new JSONObject();
        resultJson.put(CODE, errorEnum.getCode());
        resultJson.put(MESSAGE, errorEnum.getMessage());
        resultJson.put(DATA, new JSONObject());
        return resultJson;
    }
}
