package com.example.demo.common.enums;

public enum ErrorEnum {
    
    E_400("400", "请求处理异常，请稍后再试"),
    E_500("500", "请求方式有误,请检查 GET/POST"),
    E_501("501", "请求路径不存在"),
    E_502("502", "权限不足"),
    E_10008("10008", "角色删除失败,尚有用户属于此角色"),
    E_10009("10009", "账户已存在"),
    E_10010("10010", "密码为数字，小写字母，大写字母，特殊符号 至少包含三种，长度为 6 - 18位"),
    E_10011("10011", "工号已存在"),
    E_20011("20011", "登录已过期,请重新登录"),
    E_90003("90003", "缺少必填参数"),
    E_90005("90005", "参数错误"),
    E_90006("90006", "下载失败"),
    E_90007("90007", "观测点错误"),
    E_90008("90008", "上传失败"),
    E_90009("90009", "配置错误"),
    /**
     * TODO code 需要重新定义
     */
    E_PROMPT("90005", "用户提示信息，由开发者填充"),
    ;
    
    private final String code;
    
    private final String message;
    
    ErrorEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }
    
    public String getCode() {
        return code;
    }
    
    public String getMessage() {
        return message;
    }
}
