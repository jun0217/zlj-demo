package com.example.demo.common.utils;

import com.google.common.collect.HashBasedTable;
import org.apache.commons.collections.map.MultiKeyMap;

/**
 * @author 14589
 * @Des TODO
 * @Date 2023/7/21 15:24
 **/
public class Utils {
    public void methods() {
        //虽然是Table, 但其实和map的使用方式完全一致, 也能很方便的取出rowKeySet和columnKeySet, 如果是两个key的话, 推荐此方式, 代码几乎和map的写法差不多, 缺点就是只支持两个key, 三个key以上就很尴尬
        MultiKeyMap multiKeyMap = new MultiKeyMap();
        multiKeyMap.put("key1", "key2", "key3");
        
        //最多一次支持传5个key,有个最大的问题就是此方法不支持对象类型, key和value都会被视为Object, 用起来就没有Java面向对象的感觉了, 往往还需要类型转换才行
        HashBasedTable<Object, Object, Object> basedTable = HashBasedTable.create();
        basedTable.put("key1", "key2", "key3");
        
        
        
        
    }
}
